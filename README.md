tar_stream
==========

tar_stream is a python implementation of tar algorithm to archive files in chunks using tarfile

The main advantage of tar_strem is that you can create a tar or untar files from a 
source without in streams of data so you 
can easily send the tar files in small chunks over the network or write them to a file.


Example
-------

    from tar import tar_stream
    from tar import untar_stream

    # create a tar file and write to disk
    tar_file = '~/pdf.tar'
    tar_path = '~/pdf'
    with open(tar_file, 'w') as out:
        for s in tar_stream(tar_path):
            out.write(s)
            out.flush()
            # s can be send over network as well

    # extract data from tar
    untar_stream(tar_path, tar_file)

License
-------

**APACHE**, see 'LICENSE' for further details