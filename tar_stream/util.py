# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from StringIO import StringIO
import os


class FileStream(object):
    def __init__(self):
        self.buffer = StringIO()
        self.offset = 0

    def write(self, s):
        self.buffer.write(s)
        self.offset += len(s)

    def close(self):
        self.buffer.close()

    def tell(self):
        return self.offset

    def pop(self):
        s = self.buffer.getvalue()
        self.buffer.close()
        self.buffer = StringIO()
        return s


def get_files_in_path(path, exclude_dirs=[], exclude_files=()):
    """Return a list of files in a path with the relative path
    """
    os.chdir(path)

    files = None

    for root, dirs, files in os.walk('.', topdown=True):
        dirs[:] = [d for d in dirs if d not in exclude_dirs]
        files[:] = [f for f in files if not f.endswith(exclude_files)]
        files = [os.path.join(root, f) for f in files]

    return files

