# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import contextlib
import mmap
import os
import tarfile

from StringIO import StringIO


class FileStream(object):
    def __init__(self):
        self.buffer = StringIO()
        self.offset = 0

    def write(self, s):
        self.buffer.write(s)
        self.offset += len(s)

    def close(self):
        self.buffer.close()

    def tell(self):
        return self.offset

    def pop(self):
        s = self.buffer.getvalue()
        self.buffer.close()
        self.buffer = StringIO()
        return s


def _tar_stream(in_file, tar, buf_size=1048576):
    try:
        info = tarfile.TarInfo(in_file)  # create a tar info object
        stat = os.stat(in_file)          # OS meta data about the file
        info.uid = stat.st_uid           # user id
        info.gid = stat.st_gid           # group id of
        info.size = stat.st_size         # size of file, in bytes
        info.mtime = stat.st_mtime       # content modification time
        # info.ctime = stat.st_ctime       # metadata change time on Unix
                                         # time of creation on Windows
        info.mode = stat.st_mode         # file permissions
        tar.addfile(info)                # add info to tar without the object

        yield

        with open(in_file, 'rb') as in_fp:  # open in 'rb' to work on Windows

            with contextlib.closing(mmap.mmap(in_fp.fileno(), 0,
                                      access=mmap.ACCESS_READ)) as m:

                while True:
                    s = m.read(buf_size)

                    if s:
                        tar.fileobj.write(s)

                        yield

                    if len(s) < buf_size:
                        blocks, remainder = divmod(info.size,
                                                   tarfile.BLOCKSIZE)

                        if remainder > 0:
                            tar.fileobj.write(tarfile.NUL *
                                              (tarfile.BLOCKSIZE - remainder))

                            yield

                            blocks += 1

                        tar.offset += blocks * tarfile.BLOCKSIZE
                        break

        yield

    except (OSError, IOError):
        # happens when a syslink is broken
        print('Error processing file {0}'.format(in_file))


def tar_stream(path_or_file, exclude_dirs=[], exclude_files=()):
    streaming = FileStream()  # stream object to write the chunks
    tar = tarfile.TarFile.open(mode='w:', fileobj=streaming)

    # check if the path_or_file is a file
    if os.path.isfile(path_or_file):
        for i in _tar_stream(path_or_file, tar):
            s = streaming.pop()
            if s:
                yield s
    else:
        # if the path_or_file is a path chdir to path
        os.chdir(path_or_file)
        # iterate on the top dir to exclude directories and files
        for root, dirs, files in os.walk('.', topdown=True):
            dirs[:] = [d for d in dirs if d not in exclude_dirs]
            files[:] = [f for f in files if not f.endswith(exclude_files)]

            for file_ in files:
                file_ = os.path.join(root, file_)
                for i in _tar_stream(file_, tar):
                    s = streaming.pop()
                    if s:
                        yield s
    tar.close()


def untar_stream(restore_path, tar_path):
    # TODO: extract from tar in stream
    # TODO: accept input in chunks
    try:
        tar_ = tarfile.open(tar_path, mode='r:')
        tar_.extractall(path=restore_path)
    except (tarfile.TarError, Exception) as error:
        print('tar error: ', error)
