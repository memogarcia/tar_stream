# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import print_function

import hashlib
from tar.tar import tar_stream
from tar.util import get_files_in_path
from tar.tar import untar_stream

import time


def gen_tar_file(path, out_file):
    with open(out_file, 'wb') as t_o:
        for s in tar_stream(path):
            t_o.write(s)
            t_o.flush()


def hashfile(afile, hasher, blocksize=1048576):
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    return hasher.hexdigest()


def get_hashes(files):
    hashes = [(fname, hashfile(open(fname, 'rb'), hashlib.sha256()))
              for fname in files]
    return hashes


if __name__ == '__main__':
    tar_out = '/Users/memogarcia/tmp/out/out.tar'
    path = '/Users/memogarcia/tmp/in'
    path_out = '/Users/memogarcia/tmp/out'

    files = get_files_in_path(path,
                              exclude_files=('.DS_Store',))

    start = time.time()
    # create a tar file in stream
    gen_tar_file(path, tar_out)
    end = time.time()
    print('Time taken: {0}'.format(str(end-start)))

    # get the hashes from the original files
    a = get_hashes(files)

    # untar files
    untar_stream(path_out, tar_out)

    files2 = get_files_in_path(path_out,
                               exclude_files=('.DS_Store', 'out.tar'))

    # get the hashes from the extracted files
    b = get_hashes(files2)

    if a == b:
        print('ok')
    else:
        raise Exception
